package playersgames.javafile;

class Character{
    
    private final int attack;
    private final int defense;
    private final String roleName;
    private final String name_hero;
    private final int bonus_health;
    private final String id_character;
   
    
    
    Character(String id_character,int attack,int defense,int bonus_health,String roleName,String name_hero) {
        this.attack = attack;
        this.defense = defense;
        this.roleName = roleName;
        this.name_hero = name_hero;
        this.bonus_health = bonus_health;
        this.id_character = id_character;
    }
    
    public String getId_character() {
        return id_character;
    }
    public String getName() {
        return name_hero;
    }
    
    public String getRoleName() {
        return roleName;
    }
    
    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }

    public int getBonus_health() {
        return bonus_health;
    }

}
