package playersgames.javafile;

class Fighter extends Human implements Role{

    private final int ultimatePower;
    
    Fighter(String id_character,int attack,int defense,int bonus_health,String roleName,String name_hero, int health, String gender, int ultimatePower, int magazineSize, String nameWeapon, String modelWeapon, String tipePeluru) {
        super(id_character, attack, defense, bonus_health, roleName, name_hero, health, gender, magazineSize, nameWeapon, modelWeapon, tipePeluru);
        this.ultimatePower = ultimatePower;
    }
    
    public int getUltimatePower() {
        return ultimatePower;
    }

    @Override
    public void displayCharacater() {
        System.out.print("    | ID Character     : " + super.getId_character() + "\n");
        System.out.print("    +-------------------------------------------|\n");
        System.out.print("    | Type Caharacter  : " + super.getGender() + "\n");
        System.out.print("    +-------------------------------------------|\n");
        System.out.print("    | Name Caharacter  : " + super.getName() + "\n");
        System.out.print("    +-------------------------------------------|\n");
        System.out.print("    | Health Character : " + super.getHealth() + " + " + super.getBonus_health() + " Points\n");
        System.out.print("    +-------------------------------------------|\n");
        System.out.print("    | Role Character   : " + super.getRoleName().toUpperCase() + "\n");
        System.out.print("    +-------------------------------------------|\n");
        System.out.print("    | Attack Hit       : " + super.getAttack() + " Hit Points\n");
        System.out.print("    +-------------------------------------------|\n");
        System.out.print("    | Defense Armor    : " + super.getDefense() + " Points\n");
        System.out.print("    +-------------------------------------------|\n");
        System.out.print("    | Ultimate Power   : " + getUltimatePower() + " Hit Points\n");
        System.out.print("    +-------------------------------------------+\n");
    }

    @Override
    public void displayWeapon() {
        System.out.print("    | Name Weapon      : " + getNameWeapon() + "\n");
        System.out.print("    +-------------------------------------------+\n");
        System.out.print("    | Weapon Type      : " + getModelWeapon() + "\n");
        System.out.print("    +-------------------------------------------+\n");
        System.out.print("    | Magazine Size    : " + getMaxAmmo() + " Bullet\n");
        System.out.print("    +-------------------------------------------+\n");
        System.out.print("    | Bullet Type      : " + getTipePeluru() + " Ammo \n");
        System.out.print("    +++++++++++++++++++++++++++++++++++++++++++++\n");
    }
    
}
