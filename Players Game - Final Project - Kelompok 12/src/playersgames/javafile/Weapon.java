package playersgames.javafile;

interface Weapon {
    public int getMaxAmmo();
    public String getTipePeluru();
    public String getNameWeapon();
    public String getModelWeapon();
}
