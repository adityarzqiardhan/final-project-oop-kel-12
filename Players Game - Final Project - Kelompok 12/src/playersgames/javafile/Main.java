package playersgames.javafile;

import java.io.*;
import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Main {
    
    ArrayList<Login> signUser = new ArrayList<>();
    ArrayList<Fighter> fighterChar = new ArrayList<>();
    ArrayList<Assasins> assasinsChar = new ArrayList<>();
    
    Random rand = new Random();
    Scanner scan = new Scanner(System.in);
    InputStreamReader data = new InputStreamReader(System.in);  
    BufferedReader players = new BufferedReader(data);
    
    String email, password, nameHero, id_character, id_user, role, idDelete, gender, weapon;
    int index, flag=0, flagUpdate=0, flagUpdateCharacter = 0, flagSearchCharacter = 0, position = 0, i,j,k,l;
    String id_updateLog, id_updateCaharacter, nama_updateCaharacter, id_characterUpdate, id_userUpdate, name_searchCaharacter;
    int menu, cobaLagi,health, attack, bonus_health,defense, ultimatePower, inputCharScan, inputMenuUpdate, inputMenuUpdateUser;
    
    Fighter charFighter;
    Assasins charAssasins;
    Login user, userUpdate;
    
    //Class Main
    Main() {
         
        displayLogin();
        do {
    
            //Menu Pilihan
            System.out.print("\n =====================================");
            System.out.print("\n   FINAL PROJECT - KELOMPOK 12 - PBO");
            System.out.print("\n =====================================");
            System.out.print("\n 1. Add Character ");
            System.out.print("\n\n 2. Delete Character ");
            System.out.print("\n\n 3. Update Character");
            System.out.print("\n\n 4. Database Character");
            System.out.print("\n\n 5. Search Data Character");
            System.out.print("\n\n +++++++++++++++++++++++++");
            System.out.print("\n\n 6. Update Akun User");
            System.out.print("\n\n 7. Database Account User");
            System.out.print("\n\n 8. Finish Program");
            System.out.print("\n -------------------------------------");
            System.out.print("\n Pilih Menu : ");
            menu = scan.nextInt();

            switch(menu) {
                
                case 1:
                    inserthero();
                break;
                
                case 2:
                    deleteCharacter();
                break;
                
                case 3:
                    updateDataCharacter();
                break;
                
                case 4:
                    databaseCharacter();                   
                break;
                
                case 5:
                    sequentialSearchDataCharacter();                  
                break;
                
                case 6:
                   updateAccountUser();
                break;
                
                case 7:
                    viewAccountUser();
                break;
                
                case 8:
                    System.out.println("\n Terima Kasih...");
                break;
                
                default:
                    System.out.println("\n Inputan Salah!!!");
                break;    
                
            }

            //Pop Up Pengulangan Program
            System.out.print("\n ======================================= \n"); 
            System.out.print("\n  Coba Menu Lainnya ? ");
            System.out.print("\n\n  1. Ya ");
            System.out.print("\n\n  2. Keluar Program \n\n");
            System.out.println("---------------------------------------");
            do {
                System.out.print(" Pilih Dengan Angka : ");
                cobaLagi = scan.nextInt();

                if(cobaLagi == 2) {
                    System.out.println("");
                    System.out.println("-------------------------------------------------------");
                    System.out.println(" Program Telah Selesai");
                    System.out.println("");
                }
            } while((cobaLagi!=1) && (cobaLagi!=2));
        } while((cobaLagi==1));
    }
    
    //Method Display Login
    private void displayLogin() {
        do {
            System.out.print("\n\n ===========================");
            System.out.print("\n       SIGN - IN GAMES ");
            System.out.print("\n ---------------------------");
            System.out.print("\n Masukkan Username : ");
            try {
                email = players.readLine();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.print("\n Masukkan Password : ");
            try {
                password = players.readLine();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while(password.length() < 8 || password.length() >30);
        id_user = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
        user = new Login(email,password,id_user);
        signUser.add(user);
    }
    
    //Method viewAccountUser
    private void viewAccountUser() {
        //foreach untuk menampilkan data user
        System.out.print("\n ======================================="); 
        System.out.print("\n        TAMPIL DATA AKUN USER");
        System.out.print("\n ======================================= \n");  
        signUser.forEach(login -> {
            System.out.println("\n ID User : " + login.getId_user() + "\n\n Username : @" + login.getUsername().toLowerCase()
                    + "\n\n Password : " + login.getPassword());
        });
    }
              
    //Method Update Account User
    private void updateAccountUser() {
        viewAccountUser();
        System.out.print("\n\n ========================================");
        System.out.print("\n           UPDATE CHARACTER SET ");
        System.out.print("\n ========================================");
        System.out.print("\n  1. Edit Username ");
        System.out.print("\n\n  2. Edit Password ");
        System.out.print("\n ----------------------------------------");
        System.out.print("\n  Pilih Yang akan di-Update [ 1- 2 ] : ");
        inputMenuUpdateUser = scan.nextInt();
        if (inputMenuUpdateUser == 1) {
            System.out.print("\n ---------------------------------------");  
            System.out.print("\n Insert Id User Account : ");
            try {
                id_updateLog = players.readLine();
            }   catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
                if (id_updateLog.length() == 0) {
                            System.out.println("\n ID User Tidak Ditemukan !!!");
                }   else { 
                        for ( k = 0; k < signUser.size(); k++){
                            if (signUser.get(k).getId_user().contains(id_updateLog)){
                                flagUpdate = 1;
                                do {
                                    System.out.print("\n\n ===========================");
                                    System.out.print("\n     UPDATE ACCOUNT USER ");
                                    System.out.print("\n ---------------------------");
                                    System.out.print("\n Masukkan Username : ");
                                    try {
                                        email = players.readLine();
                                    } catch (IOException ex) {
                                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                } while(email.length() < 1);
                                id_user = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                userUpdate = new Login(email,password,id_user);
                                signUser.set(k, userUpdate);
                                System.out.print(" ======================================="); 
                                System.out.println("\n\n [ Update Data Akun User Berhasil ]");
                                break;
                            }
                        }
                        if (flagUpdate == 0) {
                            System.out.print("\n ID User Tidak Ditemukan !!!");
                        }
                }
        }   else if(inputMenuUpdateUser == 2) {
                System.out.print("\n ---------------------------------------");  
                System.out.print("\n Insert Id User Account : ");
                try {
                    id_updateLog = players.readLine();
                }   catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                    if (id_updateLog.length() == 0) {
                                System.out.println("\n ID User Tidak Ditemukan !!!");
                    }   else { 
                            for ( k = 0; k < signUser.size(); k++){
                                if (signUser.get(k).getId_user().contains(id_updateLog)){
                                    flagUpdate = 1;
                                    do {
                                        System.out.print("\n\n ===========================");
                                        System.out.print("\n     UPDATE ACCOUNT USER ");
                                        System.out.print("\n ---------------------------");
                                        System.out.print("\n Masukkan Password : ");
                                        try {
                                            password = players.readLine();
                                        } catch (IOException ex) {
                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    } while(password.length() < 8 || password.length() >30);
                                    id_user = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                    userUpdate = new Login(email,password,id_user);
                                    signUser.set(k, userUpdate);
                                    System.out.print(" ======================================="); 
                                    System.out.println("\n\n [ Update Data Akun User Berhasil ]");
                                    break;
                                }
                            }
                            if (flagUpdate == 0) {
                                System.out.print("\n ID User Tidak Ditemukan !!!");
                            }
                    }
        }
        
    }
    
    //Method Insert Character
    private void inserthero() {
        
        System.out.print("\n ========================================"); 
        System.out.print("\n         TAMBAH CHARACTER GAMES");
        System.out.print("\n ======================================== ");
        System.out.print("\n  1. Man Character \n");
        System.out.print("\n  2. Woman Character");
        System.out.print("\n ====================");
        System.out.print("\n  Pilih Gender : ");
        inputCharScan = scan.nextInt();

        switch (inputCharScan) {
            case 1:
                System.out.print("\n ========================================");
                System.out.print("\n         TAMBAH CHARACTER GAMES");
                System.out.print("\n ========================================");
                do {
                    System.out.print("\n Insert Character Name [50 Character] : ");
                    try {
                        nameHero = players.readLine();
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } while (nameHero.length() <= 2 || nameHero.length() >= 50);
                do {
                    System.out.print("\n Role Character [ Fighter | Assasins ] : ");
                    try {
                        role = players.readLine();
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } while (!role.equals("Fighter") && !role.equals("Assasins"));
                do {
                    System.out.print("\n Weapon [ AWM | DP-28 ] : ");
                    try {
                        weapon = players.readLine();
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } while (!weapon.equals("AWM") && !weapon.equals("DP-28"));
            switch (role) {
                case "Fighter":
                    switch(weapon) {
                        case "AWM" :
                            id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                            charFighter = new Fighter(id_character,2000,100,500,role,nameHero,2000,"Male",450,5,"AWM","Sinper Rifle","300");
                            fighterChar.add(charFighter);
                            break;
                        case "DP-28" :
                            id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                            charFighter = new Fighter(id_character,2000,100,500,role,nameHero,2000,"Male",450,47,"DP-28","Light Machine Gun","7.62");
                            fighterChar.add(charFighter);
                            break;
                        default : 
                            System.out.print("\n Weapon Tidak Ditemukan !!!");
                            break;
                    }   
                    break;
                case "Assasins":
                    switch(weapon) {
                        case "AWM" :
                            id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                            charAssasins = new Assasins(id_character,1700,100,300,role,nameHero,1700,"Male",400,5,"AWM","Sinper Rifle","300");
                            assasinsChar.add(charAssasins);
                            break;
                        case "DP-28" :
                            id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                            charAssasins = new Assasins(id_character,1700,100,300,role,nameHero,1700,"Male",400,47,"DP-28","Light Machine Gun","7.62");
                            assasinsChar.add(charAssasins);
                            break;
                        default : 
                            System.out.print("\n Weapon Tidak Ditemukan !!!");
                            break;
                    }  
                    break;
                default:
                    System.out.print("\n Role Character Tidak Ditemukan !!!");
                    break;
            }
                System.out.print(" ======================================="); 
                System.out.println("\n\n [ Data Character Telah Tersimpan ]");
                break;

                
            case 2:
                System.out.print("\n ========================================");
                System.out.print("\n         TAMBAH CHARACTER GAMES");
                System.out.print("\n ========================================");
                do {
                    System.out.print("\n Insert Character Name [50 Character] : ");
                    try {
                        nameHero = players.readLine();
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } while (nameHero.length() <= 1 || nameHero.length() >= 50);
                do {
                    System.out.print("\n Role Hero [ Fighter | Assasins ] : ");
                    try {
                        role = players.readLine();
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } while (!role.equals("Fighter") && !role.equals("Assasins"));
                    
                do {
                    System.out.print("\n Weapon [ AWM | DP-28 ] : ");
                    try {
                        weapon = players.readLine();
                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } while (!weapon.equals("AWM") && !weapon.equals("DP-28"));    
                switch (role) {
                    case "Fighter":
                        switch(weapon) {
                            case "AWM" :
                                id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                charFighter = new Fighter(id_character,1600,100,400,role,nameHero,1650,"Female",420,5,"AWM","Sinper Rifle","300");
                                fighterChar.add(charFighter);
                                break;
                            case "DP-28" :
                                id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                charFighter = new Fighter(id_character,1600,100,400,role,nameHero,1650,"Female",420,47,"DP-28","Light Machine Gun","7.62");
                                fighterChar.add(charFighter);
                                break;
                            default : 
                                System.out.print("\n Weapon Tidak Ditemukan !!!");
                                break;
                        }  
                        break;
                    case "Assasins":
                        switch(weapon) {
                            case "AWM" :
                                id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                charAssasins = new Assasins(id_character,1350,100,350,role,nameHero,1400,"Female",370,5,"AWM","Sinper Rifle","300");
                                assasinsChar.add(charAssasins);
                                break;
                            case "DP-28" :
                                id_character = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                charAssasins = new Assasins(id_character,1350,100,350,role,nameHero,1400,"Female",370,47,"DP-28","Light Machine Gun","7.62");
                                assasinsChar.add(charAssasins);
                                break;
                            default : 
                                System.out.print("\n Weapon Tidak Ditemukan !!!");
                                break;
                        }
                        break;
                    default:
                        System.out.print("\n Role Character Tidak Ditemukan !!!");
                        break;
                }
                    System.out.print(" ======================================="); 
                    System.out.println("\n\n [ Data Character Telah Tersimpan ]");
                break;
                
            default:
                System.out.println("\n Inputan Salah !!!, Harap Masukkan yang Benar !");
                break;
        }
    }
    
    //Method Delete Data Character 
    private void deleteCharacter() {
        if(fighterChar.isEmpty() && assasinsChar.isEmpty()) {
            System.out.println("\n Tidak ada Data Character yang dapat dihapus !!!");
        } else {
            databaseCharacter();
                System.out.print("\n Insert Id Character : ");
                try {
                    idDelete = players.readLine();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                for (i = 0; i < fighterChar.size(); i++){
                    if (fighterChar.get(i).getId_character().contains(idDelete)){
                        flag=1;
                        fighterChar.remove(i);
                        i--;
                        System.out.print(" ======================================="); 
                        System.out.println("\n\n [ Data Character dengan Id " + idDelete + " Telah Terhapus ]");
                        break;
                    }

                }
                for (i = 0; i < assasinsChar.size(); i++){
                    if (assasinsChar.get(i).getId_character().contains(idDelete)){
                        flag=1;
                        assasinsChar.remove(i);
                        i--;
                        System.out.print(" ======================================="); 
                        System.out.println("\n\n [ Data Character dengan Id " + idDelete + " Telah Terhapus ]");
                        break;
                    }   
                }
                if (idDelete.length() == 0 && flag == 0){
                    System.out.println("\n ID Character Tidak Ditemukan !!!");
                }
        }
    }
    
    //Method Update Data Character
    
    private void updateDataCharacter() {
        if (fighterChar.isEmpty() && assasinsChar.isEmpty()) {
             System.out.println("\n Tidak ada Data Character yang dapat di-Update !!!");
        } else {
            databaseCharacter();
            System.out.print("\n\n ========================================");
            System.out.print("\n           UPDATE CHARACTER SET ");
            System.out.print("\n ========================================");
            System.out.print("\n  1. Update Nama Character");
            System.out.print("\n ----------------------------------------");
            System.out.print("\n  Pilih Yang akan di-Update : ");
            inputMenuUpdate = scan.nextInt();
            if (inputMenuUpdate == 1) {
                System.out.print("\n ---------------------------------------");  
                System.out.print("\n Insert Id Character : ");
                try {
                    id_updateCaharacter = players.readLine();
                }   catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                    if (id_updateCaharacter.length() == 0) {
                                System.out.println("\n ID User Tidak Ditemukan !!!");
                    }   else { 

                            for (  i=0; i < fighterChar.size(); i++){
                                if (fighterChar.get(i).getId_character().contains(id_updateCaharacter)){ 
                                    if (fighterChar.get(i).getRoleName().contains("Fighter")) {
                                        if (fighterChar.get(i).getGender().contains("Male")) {
                                            if (fighterChar.get(i).getNameWeapon().contains("AWM")) {
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charFighter = new Fighter(id_characterUpdate,200,100,500,role,nama_updateCaharacter,2000,"Male",450,5,"AWM","Sinper Rifle","300");
                                                fighterChar.set(i, charFighter);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                            else if (fighterChar.get(i).getNameWeapon().contains("DP-28")){
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charFighter = new Fighter(id_characterUpdate,200,100,500,role,nama_updateCaharacter,2000,"Male",450,47,"DP-28","Light Machine Gun","7.62");
                                                fighterChar.set(i, charFighter);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                        }
                                        else if (fighterChar.get(i).getGender().contains("Female")) {
                                            if (fighterChar.get(i).getNameWeapon().contains("AWM")) {
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charFighter = new Fighter(id_characterUpdate,1600,100,400,role,nama_updateCaharacter,1650,"Female",420,5,"AWM","Sinper Rifle","300");
                                                fighterChar.set(i, charFighter);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                            else if (fighterChar.get(i).getNameWeapon().contains("DP-28")){
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charFighter = new Fighter(id_characterUpdate,1600,100,400,role,nama_updateCaharacter,1650,"Female",420,47,"DP-28","Light Machine Gun","7.62");
                                                fighterChar.set(i, charFighter);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                        }
                                    } 
                                }
                            }

                            for ( j=0; j < assasinsChar.size(); j++){
                                if (assasinsChar.get(j).getId_character().contains(id_updateCaharacter)){ 
                                    if (assasinsChar.get(j).getRoleName().contains("Assasins")) {
                                        if (assasinsChar.get(j).getGender().contains("Male")) {
                                            if (assasinsChar.get(j).getNameWeapon().contains("AWM")) {
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charAssasins = new Assasins(id_characterUpdate,170,100,300,role,nama_updateCaharacter,1700,"Male",400,5,"AWM","Sinper Rifle","300");
                                                assasinsChar.set(j, charAssasins);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                            else if (assasinsChar.get(j).getNameWeapon().contains("DP-28")){
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charAssasins = new Assasins(id_characterUpdate,170,100,300,role,nama_updateCaharacter,1700,"Male",400,47,"DP-28","Light Machine Gun","7.62");
                                                assasinsChar.set(j, charAssasins);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                        }
                                        else if (assasinsChar.get(j).getGender().contains("Female")) {
                                            if (assasinsChar.get(j).getNameWeapon().contains("AWM")) {
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charAssasins = new Assasins(id_characterUpdate,1350,100,350,role,nama_updateCaharacter,1400,"Female",370,5,"AWM","Sinper Rifle","300");
                                                assasinsChar.set(j, charAssasins);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                            else if (assasinsChar.get(j).getNameWeapon().contains("DP-28")){
                                                flagUpdateCharacter = 1;
                                                do {
                                                    System.out.print("\n\n ========================================");
                                                    System.out.print("\n           UPDATE CHARACTER SET ");
                                                    System.out.print("\n ========================================");
                                                    System.out.print("\n Insert Character Name [50 Character] : ");
                                                    try {
                                                        nama_updateCaharacter = players.readLine();
                                                    }   catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                    }
                                                } while (nama_updateCaharacter.length() <= 1 || nama_updateCaharacter.length() >= 50);
                                                id_characterUpdate = "" + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10) + Math.abs(rand.nextInt() % 10);
                                                charAssasins = new Assasins(id_characterUpdate,1350,100,350,role,nama_updateCaharacter,1400,"Female",370,47,"DP-28","Light Machine Gun","7.62");
                                                assasinsChar.set(j, charAssasins);
                                                System.out.print(" ======================================="); 
                                                System.out.println("\n\n [ Update Data Character Berhasil ]");
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (flagUpdateCharacter == 0) {
                                System.out.print("\n ID User Tidak Ditemukan !!!");
                            }
                        }
            }
        }
    }
    
    //Method View Database Character
    private void databaseCharacter() {
        
        if(fighterChar.isEmpty() && assasinsChar.isEmpty()) {
            System.out.println("\n Database Character Masih Belum Terisi.");
        } else {
            System.out.print("\n\n =============================================\n");
            System.out.print("            DATABASE SET CHARACTERS \n");
            System.out.print(" =============================================\n");
            System.out.println(" Jumlah Character [Type : Fighter]  : " + fighterChar.size() + " Character \n");
            System.out.println(" Jumlah Character [Type : Assasins] : " + assasinsChar.size() + " Character");
            System.out.print(" -------------------------------------------------- \n");
            
            for ( Fighter fighter : fighterChar) {
                System.out.print("\n ++++-------------------------------------------+\n");
                System.out.print(" " + ""+ "=> " + "+                 CHARACTER" + "                 +\n");
                System.out.print(" ++++-------------------------------------------+\n");
                fighter.displayCharacater();
                System.out.print("    +                   WEAPON                  +\n");
                System.out.print("    +-------------------------------------------+\n");
                fighter.displayWeapon();
            }
            for ( Assasins assasins : assasinsChar ) {
                System.out.print("\n ++++-------------------------------------------+\n");
                System.out.print(" " + "" + "=> " + "+                 CHARACTER" + "                 +\n");
                System.out.print(" ++++-------------------------------------------+\n");
                assasins.displayCharacater();
                System.out.print("    +                   WEAPON                  +\n");
                System.out.print("    +-------------------------------------------+\n");
                assasins.displayWeapon();
            }       
        }
    }
    
    private void sequentialSearchDataCharacter() {
        
        if (fighterChar.isEmpty() && assasinsChar.isEmpty()) {
            System.out.println("\n Database Character Masih Belum Terisi.");
        } else {
            System.out.print("\n =====================================\n");
            System.out.print("            SEARCH CHARACTERS \n");
            System.out.print(" ======================================"); 
            System.out.print("\n Insert Name Character : ");
            try {
                name_searchCaharacter = players.readLine();
            }   catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Fighter fighterSearch : fighterChar) {
                if (fighterSearch.getName().equals(name_searchCaharacter)) {
                    flagSearchCharacter = 1;
                    System.out.print("\n [ Data Character Ditemukan ] \n");
                    System.out.print("\n ++++-------------------------------------------+\n");
                    System.out.print(" " + "" + "=> " + "+                 CHARACTER" + "                 +\n");
                    System.out.print(" ++++-------------------------------------------+\n");
                    fighterSearch.displayCharacater();
                    System.out.print("    +                   WEAPON                  +\n");
                    System.out.print("    +-------------------------------------------+\n");
                    fighterSearch.displayWeapon();
                }
            }
            for (Assasins assasinsSearch : assasinsChar) {
                if (assasinsSearch.getName().equals(name_searchCaharacter)) {
                    flagSearchCharacter = 1;
                    System.out.print("\n [ Data Character Ditemukan ] \n");
                    System.out.print("\n ++++-------------------------------------------+\n");
                    System.out.print(" " + "" + "=> " + "+                 CHARACTER" + "                 +\n");
                    System.out.print(" ++++-------------------------------------------+\n");
                    assasinsSearch.displayCharacater();
                    System.out.print("    +                   WEAPON                  +\n");
                    System.out.print("    +-------------------------------------------+\n");
                    assasinsSearch.displayWeapon();
                }
            }
            
             if (flagSearchCharacter == 0) {
                System.out.print("\n Character Tidak Ditemukan !!!");
            }
        }
    }

    public static void main(String[] args) throws IOException {
        
        // Nama Kelompok 12 :
        // 1. Aditya Rizqi Ardhana (19081010178)
        // 2. Elsa Febriantika (19081010028)
        // 3. Muhammad Syaiful Arif (19081010141)
        // Kelas  = Pemrograman Berorientasi Objek - C
        // FINAL PROJECT
        //Judul : Mini Customize Character
        Main main = new Main();
    }
}