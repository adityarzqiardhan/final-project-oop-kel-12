package playersgames.javafile;

class Login {
    
    private final String id_user;
    private final String username;
    private final String password;
    
    public Login(String username, String password, String id_user) {
        this.id_user = id_user;
        this.username = username;
        this.password = password;
    }
    
     public String getId_user() {
        return id_user;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    
    void display() {
        
    }
   
}
