package playersgames.javafile;

class Human extends Character implements Weapon {
    
    private final int health;
    private final String gender;
    private final int magazineSize;
    private final String nameWeapon;
    private final String tipePeluru;
    private final String modelWeapon;
    
    
    Human(String id_character,int attack,int defense,int bonus_health,String roleName,String name_hero, int health, String gender, int magazineSize, String nameWeapon, String modelWeapon, String tipePeluru) {
        super(id_character, attack, defense, bonus_health, roleName, name_hero);
        this.health = health;
        this.gender = gender;
        this.nameWeapon = nameWeapon;
        this.tipePeluru = tipePeluru;
        this.modelWeapon= modelWeapon;
        this.magazineSize = magazineSize;
    }
    
    public int getHealth() {
        return health;
    }
    
    public String getGender() {
        return gender;
    }
   
    @Override
    public int getMaxAmmo() {
       return magazineSize;
    }

    @Override
    public String getNameWeapon() {
        return nameWeapon;
    }

    @Override
    public String getModelWeapon() {
        return modelWeapon;
    }

    @Override
    public String getTipePeluru() {
        return tipePeluru;
    }
}
